# NON PREEMPTIVE SJF SCHEDULER

Implementare uno *scheduler* a livello applicativo in *Java* che adotti una politica *Shortest Job First* (non preemptive).

Creare una classe `SJFScheduler` che, dato un insieme di thread, inizializzati con la durata del picco (*burst*), gestisca la coda di esecuzione e registri per ogni thread eseguito alcune statistiche:

- *waiting* time
- *turnaround* time

Infine quando tutti i thread sono terminati emettere su **stdout**:

- *average waiting* time
- *average turnaround* time