package it.unibg.so.lab7;

public class Driver {
	
	public static void main(String[] args){
		SJFScheduler scheduler = SJFScheduler.getInstance();
		scheduler.start();
		
		scheduler.schedule(new Schedulable(5) {
			@Override
			public void executeJob() {
				System.out.println("[" + toString() + "] running...");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println("[" + toString() + "] finisched...");
			}			
		});
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		scheduler.schedule(new Schedulable(7) {
			@Override
			public void executeJob() {
				System.out.println("[" + toString() + "] running...");
				try {
					Thread.sleep(7000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println("[" + toString() + "] finisched...");
			}			
		});
		
		scheduler.schedule(new Schedulable(3) {
			@Override
			public void executeJob() {
				System.out.println("[" + toString() + "] running...");
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println("[" + toString() + "] finisched...");
			}			
		});
		
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		scheduler.interrupt();
	}

}
