package it.unibg.so.lab7;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SJFScheduler extends Thread {
	
	private static final String NAME = "SJF-scheduler";
	
	private static SJFScheduler scheduler = null;
	private List<Schedulable> queue;
	private Comparator<Schedulable> comparator;
	
	private Lock mutex;
	private Condition waitForComplition;
	private Condition waitForJob;
	
	private List<Long> waitingTime;
	private List<Long> turnaroundTime;
	
	private SJFScheduler(){
		queue = new ArrayList<Schedulable>();
		comparator = new SchedulableComparator();
		mutex = new ReentrantLock();
		waitForComplition = mutex.newCondition();
		waitForJob = mutex.newCondition();
		setName(NAME);
		
		waitingTime = new ArrayList<Long>();
		turnaroundTime = new ArrayList<Long>();
	}
	
	public static synchronized SJFScheduler getInstance(){
		if(scheduler == null)
			scheduler = new SJFScheduler();
		return scheduler;
	}
	
	public void schedule(Schedulable t){
		mutex.lock();
		try{
			t.setScheduleTime(System.currentTimeMillis());
			queue.add(t);
			Collections.sort(queue, comparator);
			waitForJob.signal();
			System.out.println("[" + Thread.currentThread().getName() + "] new job signal...");
		} finally {
			mutex.unlock();
		}
	}
	
	private void executeShortestJob(){
		mutex.lock();
		try{
			if(!queue.isEmpty()){
				Schedulable t = queue.remove(0);
				Collections.sort(queue, comparator);
				System.out.println("[" + Thread.currentThread().getName() + "] shortest job execution...");
				t.setExecutionTime(System.currentTimeMillis());
				t.start();
				try {
					System.out.println("[" + Thread.currentThread().getName() + "] await for completion...");
					waitForComplition.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} finally {
			mutex.unlock();
		}
	}
	
	@Override
	public void run(){
		mutex.lock();
		System.out.println("[" + Thread.currentThread().getName() + "] SJF scheduler running...");
		try{
			boolean interrupted = false;
			while(!interrupted){
				while(queue.isEmpty() && !interrupted){
					try {
						System.out.println("[" + Thread.currentThread().getName() + "] await for job...");
						waitForJob.await();
					} catch (InterruptedException e) {
						System.out.println("[" + Thread.currentThread().getName() + "] interrupted...");
						interrupted = true;
					}
				}
				executeShortestJob();
			}			
			
		} finally {
			mutex.unlock();
		}
		
		double avgWatingTime = 0;
		double avgTurnaroundTime = 0;
		
		for(int i=0; i<waitingTime.size(); i++){
			avgWatingTime += waitingTime.get(i);
			avgTurnaroundTime += turnaroundTime.get(i);
		}
		
		System.out.println("[" + Thread.currentThread().getName() + "] average wating time: " + avgWatingTime/waitingTime.size());
		System.out.println("[" + Thread.currentThread().getName() + "] average turnaround time: " + avgTurnaroundTime/turnaroundTime.size());
	}

	public void jobFinished(Schedulable t) {
		mutex.lock();
		try{
			t.setShutdownTime(System.currentTimeMillis());
			
			waitingTime.add(t.getExecutionTime() - t.getScheduleTime());
			turnaroundTime.add(t.getShutdownTime() - t.getScheduleTime());
			
			waitForComplition.signal();
			System.out.println("[" + Thread.currentThread().getName() + "] completion signal...");
		} finally {
			mutex.unlock();
		}
	}

}
