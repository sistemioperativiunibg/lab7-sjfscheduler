package it.unibg.so.lab7;

import java.util.Comparator;

public class SchedulableComparator implements Comparator<Schedulable> {

	@Override
	public int compare(Schedulable o1, Schedulable o2) {
		return o1.getBurst() - o2.getBurst();
	}

}
