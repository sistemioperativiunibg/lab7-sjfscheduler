package it.unibg.so.lab7;

public abstract class Schedulable extends Thread {
	
	private Integer burst;
	
	protected Long scheduleTime;
	protected Long executionTime;
	protected Long shutdownTime;
	
	public Schedulable(Integer burst){
		this.burst = burst;
	}

	public Integer getBurst() {
		return burst;
	}

	public void setBurst(Integer burst) {
		this.burst = burst;
	}
	
	private void shutDownExecution(){
		SJFScheduler.getInstance().jobFinished(this);
	}

	@Override
	public void run() {
		super.run();
		executeJob();
		shutDownExecution();
	}
	
	public abstract void executeJob();

	public Long getScheduleTime() {
		return scheduleTime;
	}

	public void setScheduleTime(Long scheduleTime) {
		this.scheduleTime = scheduleTime;
	}

	public Long getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(Long executionTime) {
		this.executionTime = executionTime;
	}

	public Long getShutdownTime() {
		return shutdownTime;
	}

	public void setShutdownTime(Long shutdownTime) {
		this.shutdownTime = shutdownTime;
	}
	
	@Override
	public String toString(){
		return Thread.currentThread().getName() + " (burst " + getBurst() + ")";
	}
	
}
